import React, { Component } from 'react';
import './App.css';
import Person from '../Components/Person/Person';

class App extends Component {

  state = {
    persons: [
      {id: 1, name: "Orlando Whyte", age: 26},
      {id: 2, name: "Renee Dujoy", age: 27},
      {id: 3, name: "Alexis Whyte", age: 2}
    ],
    showPersons: false
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons: [
        {name: event.target.value, age: 2},
        {name: "Renee Dujoy", age: 27},
        {name: "Alexis Whyte", age: 2}
      ]
    });
  }

  toggleShowPerson = () => {
    let show = this.state.showPersons;
    this.setState({
      showPersons: !show
    });
  }

  render() {

    let persons = null;
    if(this.state.showPersons){
      persons = (
        <div>
          {this.state.persons.map(person => {
            return <Person 
            name={person.name} 
            age={person.age} 
            key={person.id}
            />
          })}
        </div>
      );
    }

    const style = {
      backgroundColor: 'blue',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px'
    }

    return (
      <div className="App">
          <h1>Hi, Im a React App</h1>
          <button style={style} onClick={this.toggleShowPerson}>Toggle Persons</button>
          {persons}
      </div>
    );
  }
}

export default App;
